const randomNumber = (max = 9999, min = 1000) => Math.floor((Math.random() * (max - min)) + min);
export default randomNumber;
