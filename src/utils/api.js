import { ApolloClient, HttpLink, InMemoryCache } from "@apollo/client";

const API = new ApolloClient({
  link: new HttpLink({
    uri: 'https://graphql-pokeapi.graphcdn.app/',
  }),
  cache: new InMemoryCache()
});

export default API
