import React from 'react';
import styled from '@emotion/styled';
import Card from 'components/Card';
import Link from 'components/Link'
import Button from 'components/Button';
import Modal from 'components/Modal';

const Container = styled.div`
margin: 0px 8px;
& .card{
  margin-bottom: 8px;
}
& .image {
  display: flex;
  margin: auto;
}
`

const MyPokemonList = () => {
  const [myPokemons, setMyPokemons] = React.useState(null)
  const [showRelease, setShowRelease] = React.useState(false)
  const [selectedPokemon, setSelectedPokemon] = React.useState('')

  const handleShowRelease = (e, name) => {
    e.preventDefault();
    setSelectedPokemon(name)
    setShowRelease(true)
  }

  const handleRelease = (type) => {
    if (type) {
      const nextPokemon = myPokemons.filter(item => item.name !== selectedPokemon)
      setMyPokemons(nextPokemon)
      localStorage.setItem('MyPokemon', JSON.stringify(nextPokemon))
    }
    setSelectedPokemon('')
    onCloseRelease()
  }

  const onCloseRelease = () => {
    setShowRelease(false)
  }

  React.useEffect(() => {
    const myPokemonLocal = localStorage.getItem('MyPokemon')
    if (myPokemonLocal) {
      setMyPokemons(JSON.parse(myPokemonLocal))
    }
  }, [])

  return (
    <Container>
      {myPokemons?.map(pokemon => (
        <Link to={`/pokemon/${pokemon?.pokemon}`} key={pokemon.name} >
          <Card className='card' >
            <img src={pokemon.image} alt={pokemon.name} className='image' width='96px' height='96px' />
            <div>{pokemon.name}</div>
            <Button fullWidth color='#1976d2' background='white' onClick={(e) => handleShowRelease(e, pokemon.name)}>
              Release
            </Button>
          </Card>
        </Link>
      ))}
      <Modal show={showRelease} onClose={onCloseRelease}>
        <Card background='white'>
          <div>Anda yakin ingin melepaskan {selectedPokemon}?</div>
          <Button fullWidth color='#1976d2' background='white' onClick={() => handleRelease(true)}>
            Ya
          </Button>
          <Button fullWidth onClick={() => handleRelease(false)}>
            Tidak
          </Button>
        </Card>
      </Modal>
    </Container>
  );
}

export default MyPokemonList;
