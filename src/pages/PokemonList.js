import React from 'react';
import GetPokemonList from 'services/PokemonListAPI';
import styled from '@emotion/styled';
import Card from 'components/Card';
import Link from 'components/Link'

const Container = styled.div`
margin: 0px 8px;
& .card{
  margin-bottom: 8px;
}
& .image {
  display: flex;
  margin: auto;
}
`

const PokemonList = () => {
  const [pokemons, setPokemons] = React.useState([])
  const [myPokemons, setMyPokemons] = React.useState(null)

  React.useEffect(() => {
    const myPokemonLocal = localStorage.getItem('MyPokemon')
    console.log('debug myPokemonLocal', myPokemonLocal)
    if (myPokemonLocal) {
      setMyPokemons(JSON.parse(myPokemonLocal))
    }
    GetPokemonList()
      .then(result => {
        setPokemons(result.data?.pokemons?.results)
      })
      .catch(() => setPokemons([]));
    return (() => setPokemons([]))
  }, [])

  return (
    <Container>
      {pokemons?.map(pokemon => (
        <Link to={`/pokemon/${pokemon.name}`} key={pokemon.id} >
          <Card className='card' >
            <img src={pokemon.image} alt={pokemon.name} className='image' width='96px' height='96px' />
            <div>{pokemon.name}</div>
            <div>owned total: {myPokemons && myPokemons.filter(item => item.pokemon === pokemon.name).length}</div>
          </Card>
        </Link>
      ))}
    </Container>
  );
}

export default PokemonList;
