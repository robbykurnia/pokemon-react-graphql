import styled from '@emotion/styled';
import Button from 'components/Button';
import Input from 'components/Input';
import Card from 'components/Card';
import Modal from 'components/Modal';
import React from 'react';
import { useParams } from 'react-router-dom';
import PokemonDetailAPI from 'services/PokemonDetailAPI';
import randomNumber from 'utils/randomNumber';

const Container = styled.div`
margin: 0px 8px;
& > * {
  padding: 8px 0px;
  border-bottom: 1px solid #E7EBF0;
}
& .image {
  display: flex;
  margin: auto;
}

& .label {
  font-weight: 700;
}
`
const PokemonDetail = () => {
  const { name } = useParams();
  const [detail, setDetail] = React.useState(null)
  const [showInputName, setShowInputName] = React.useState(false)
  const [inputName, setInputName] = React.useState('')
  const [isCatching, setIsCatching] = React.useState(false)
  const [isAlreadyUse, setIsAlreadyUse] = React.useState(false)
  const [ownedTotal, setOwnedTotal] = React.useState(0)
  const CATCHING = 'Catching...'
  const CATCH = 'Catch'

  const onCloseInputName = () => {
    setInputName('')
    setIsAlreadyUse(false)
    setShowInputName(false)
  }

  const handleSavePokemon = () => {
    const isOwnComponent = localStorage.getItem('MyPokemon')
    if (isOwnComponent) {
      const currentPokemon = JSON.parse(isOwnComponent)
      const isSameName = currentPokemon.find(item => item.name === inputName)
      if (!isSameName) {
        currentPokemon.push({ name: inputName, image: detail.sprites.front_default, pokemon: name })
        localStorage.setItem('MyPokemon', JSON.stringify(currentPokemon))
        onCloseInputName()
        setOwnedTotal((prev) => prev + 1)
      } else {
        setIsAlreadyUse(true)
      }
    } else {
      setOwnedTotal(1)
      const value = [{ name: inputName, image: detail.sprites.front_default, pokemon: name }]
      localStorage.setItem('MyPokemon', JSON.stringify(value))
      onCloseInputName()
    }
  }

  const handleCatch = () => {
    setIsCatching(true)
    setTimeout(() => {
      const caught = randomNumber(0, 2)
      if (caught === 1) {
        setShowInputName(true)
      }
      setIsCatching(false)
    }, 300)
  }

  React.useEffect(() => {
    const myPokemonLocal = localStorage.getItem('MyPokemon')
    if (myPokemonLocal) {
      const currentPokemon = JSON.parse(myPokemonLocal)
      setOwnedTotal(currentPokemon.filter(item => item.pokemon === name).length)
    }
    PokemonDetailAPI(name)
      .then((res) => {
        setDetail(res.data.pokemon)
      })
      .catch(() => setDetail(null))
  }, [name])

  return detail && (
    <Container>
      <div>
        <img src={detail.sprites.front_default} alt={detail.name} className='image' width='96px' height='96px'/>
        <div className='label'>{detail.name}</div>
        <Button fullWidth disabled={isCatching} onClick={handleCatch}>
          {isCatching ? CATCHING : CATCH}
        </Button>
      </div>


      <div>
        Owned total : {ownedTotal}
      </div>

      <div>
        <div className='label'>Moves</div>
        {detail?.moves?.map(item => (
          <div key={item.move?.name}>
            {item.move?.name}
          </div>
        ))}
      </div>

      <div>
        <div className='label'>Types</div>
        {detail?.types?.map(item => (
          <div key={item.type?.name}>
            {item.type?.name}
          </div>
        ))}
      </div>
      <Modal show={showInputName} onClose={onCloseInputName}>
        <Card background='white'>
          <div>Congratulation!</div>
          <div>
            give the Pokemon a nickname
          </div>
          <Input value={inputName} onChange={(e) => setInputName(e.target.value)} />
          {isAlreadyUse && <div>Nickname telah digunakan</div>}
          <Button fullWidth onClick={handleSavePokemon}>
            Simpan
          </Button>
        </Card>
      </Modal>
    </Container>
  )
}

export default PokemonDetail;
