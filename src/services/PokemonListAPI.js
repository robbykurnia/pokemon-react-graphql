import API from 'utils/api'
import { gql } from '@apollo/client';
const PokemonListAPI = () => {
  return API
    .query({
      query: gql`
        query pokemons($limit: Int = 9, $offset: Int = 0) {
          pokemons(limit: $limit, offset: $offset) {
            count
            next
            previous
            nextOffset
            prevOffset
            status
            message
            results {
              id
              url
              name
              image
            }
          }
        }
      `
    })
}

export default PokemonListAPI;
