import API from 'utils/api'
import { gql } from '@apollo/client';
const PokemonDetailAPI = (name) => {
  return API
    .query({
      query: gql`
        query pokemon($name: String! = "${name}") {
          pokemon(name: $name) {
            id
            name
            sprites {
              front_default
            }
            moves {
              move {
                name
              }
            }
            types {
              type {
                name
              }
            }
          }
        }
      `
    })
}

export default PokemonDetailAPI;
