import React, {Suspense} from 'react';
import styled from '@emotion/styled';
import {
  BrowserRouter as Router,
  Route,
  Routes
} from 'react-router-dom';
import Header from 'components/Header';
import NotFound from 'components/NotFound';
// import MyPokemonList from 'pages/MyPokemonList';
// import PokemonDetail from 'pages/PokemonDetail';
// import PokemonList from 'pages/PokemonList';

import { lazy } from '@loadable/component'
const MyPokemonList = lazy(() => import('./pages/MyPokemonList'))
const PokemonDetail = lazy(() => import('./pages/PokemonDetail'))
const PokemonList = lazy(() => import('./pages/PokemonList'))

const Container = styled.div`
margin-top: 60px;
color: #1A2027;
`;

function App() {
  return (
    <Container data-testid="App-wrapper">
      <Suspense fallback={<div>Loading...</div>}>
        <Router>
          <Header />
          <Routes>
            <Route path="/" element={<PokemonList />} />

            <Route path="/pokemon/:name" element={<PokemonDetail />} />

            <Route path="/mypokemon" element={<MyPokemonList />} />

            <Route path="*" element={<NotFound />} />

          </Routes>
        </Router>
      </Suspense>
    </Container>
  );
}

export default App;
