import styled from '@emotion/styled';
import React from 'react';

const StyledButton = styled.button`
  color: ${({ color }) => color};
  background: ${({ background }) => background};
  width: ${({ fullWidth }) => fullWidth ? '100%' : ''};
  padding: 8px 4px;
  border-radius: 4px;
  font-weight: 700;
  border: 1px solid #1976d2;

  &:active {
    background: #1858a1;
  }
  &:disabled {
    color: rgba(0, 0, 0, 0.26);
    background-color: rgba(0, 0, 0, 0.12);
    border: none;
  }
`

const Button = ({ children, background = '#1976d2', color = 'white', className, fullWidth, ...props }) => {
  return (
    <StyledButton
      background={background}
      color={color}
      className={className}
      fullWidth={fullWidth}
      {...props}
    >
      {children}
    </StyledButton>
  );
}

export default Button;
