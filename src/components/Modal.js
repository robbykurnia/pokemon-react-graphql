import React from 'react';
import { bool, func, node } from 'prop-types';
import styled from '@emotion/styled';

const Container = styled.div`
  top: 0px;
  left: 0px;
  position: fixed;
  width: 100%;
  height: 100%;
  background: #a39c9c45;
  display: flex;
  justify-content: center;
  align-items: center;
`


const Modal = ({ show, onClose, children }) => {

  const handleBubble = (e) => {
    e.stopPropagation();
  }

  return show && (
    <Container onClick={onClose}>
      <div onClick={handleBubble}>
        {children}
      </div>
    </Container>
  );
}

Modal.propTypes = {
  show: bool,
  onClose: func,
  children: node
}
Modal.defaultProps = {
  show: false,
  onClose: () => null,
  children: null
}

export default Modal;
