import React from 'react';
import styled from '@emotion/styled';
import Link from 'components/Link';
// import { useMatch } from 'react-router-dom'

const Container = styled.div`
display: flex;
position: fixed;
top: 0;
background: white;
width: 100%;
border-bottom: 1px solid #E7EBF0;
& > * {
  padding: 1rem;
  font-weight: 700;
}

`

const Header = () => {
  // const isHome = useMatch("/");
  // const isMyPokemon = useMatch("/mypokemon");
  return (
    <Container>
      <Link to="/" >Pokemon List</Link>
      <Link to="/mypokemon">My Pokemon List</Link>
    </Container>
  );
}

export default Header;
