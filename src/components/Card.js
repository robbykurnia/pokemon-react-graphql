import styled from '@emotion/styled';
import React from 'react';

const Container = styled.div`
border: 1px solid #0000001f;
padding: 16px;
border-radius: 8px;
background: ${({ background }) => background};
& > * {
  margin-bottom: 8px;
}
`

const Card = ({ children, className, background }) => {
  return (
    <Container className={className} background={background} >
      {children}
    </Container>
  );
}

export default Card;
