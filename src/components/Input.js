import React from 'react';
import styled from '@emotion/styled';


const StyledInput = styled.input`
display: block;
width: 100%;
border-radius: 4px;
box-sizing: border-box;
padding: 4px 8px;
border: 1px solid #9e9e9e;
`

const Input = (props) => {
  return (
    <StyledInput {...props} />
  );
}

export default Input;
