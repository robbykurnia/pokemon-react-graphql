import React from 'react';
import styled from '@emotion/styled';
import { Link as LinkRouter } from 'react-router-dom';



const StyledLink = styled(LinkRouter)`
    text-decoration: none;
    color: unset;

    &:focus, &:hover, &:visited, &:link, &:active {
        text-decoration: none;
    }
`;

const Link = (props) => <StyledLink {...props} />;

export default Link;
